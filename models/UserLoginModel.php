<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class UserLoginModel extends Model {
        protected function getFields(): array {
            return [
                'user_login_id' => Field::readonlyInteger(11),
                'created_at'    => Field::readonlyDateTime(),

                'user_id'       => Field::editableInteger(11)
            ];
        }

        public function getAllByUserLoginId(int $userloginId): array {
            return $this->getAllByFieldName('userlogin_id', $userloginId);
        }
    }