<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class CategoryModel extends Model {
        protected function getFields(): array {
            return [
                'category_id' => Field::readonlyInteger(11),
                'created_at'  => Field::readonlyDateTime(),

                'name'        => Field::editableString(64),
                'measure'     => Field::editableMaxDecimal(7, 2)
            ];
        }
        
    }

   