<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class CartModel extends Model {
        protected function getFields(): array {
            return [
                'cart_id'        => Field::readonlyInteger(11),
                'created_at'     => Field::readonlyDateTime(),

                'session_number' => Field::editableString(11), #?
                'notes'          => Field::editableString(64*1024),
                'open_date'      => Field::editableDateTime()
            ];
        }

        public function getAllByCategoryId(int $categoryId): array {
            
           return $this->getAllByFieldName('category_id', $categoryId);
           
        }
    }