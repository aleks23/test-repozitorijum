<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class ProductViewModel extends Model {
        protected function getFields(): array {
            return [
                'product_view_id' => Field::readonlyInteger(20),
                'created_at'      => Field::readonlyDateTime(),
                'product_id'      => Field::editableInteger(11),
                'ip_address'      => Field::editableIpAddress(),
                'user_agent'      => Field::editableString(255)
                
            ];
        }


        public function getAllByProductId(int $productId): array {
            return $this->getAllByFieldName('product_id', $productId);
        }

        public function getAllByIpAddress(string $ipAddress): array {
            return $this->getAllByFieldName('ip_address', $ipAddress);
        }
    }

   