<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    

    class UserModel extends Model {protected function getFields(): array {
        return [
            'user_id'          => Field::readonlyInteger(11),
            'created_at'       => Field::readonlyDateTime(),
            
            'username'         => Field::editableString(64*1024),
            'email'            => Field::editableString(255),
            'password_hash'    => Field::editableString(128),  
            'is_active'        => Field::editableBit()
        ];
    }

        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }
    }