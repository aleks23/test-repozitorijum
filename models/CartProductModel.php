<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class CartProductModel extends Model {
        protected function getFields(): array {
            return [
                'cart_product_id' => Field::readonlyInteger(11),
                'addet_at'        => Field::readonlyDateTime(),

                'cart_id'         => Field::editableInteger(11),
                'product_id'      => Field::editableInteger(11),
                'measure'         => Field::editableMaxDecimal(7, 2)
            ];
        }

        public function getAllByProductId(int $productId): array { 
            
            $items = $this->getAllByFieldName('product_id', $productId);

            usort($items, function($a, $b) {
                return strcmp($a->addet_at, $b->addet_at);
           });
           
           return $items;

        }
    }
