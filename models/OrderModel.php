<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class OrderModel extends Model {
        protected function getFields(): array {
            return [
                'order_id'         => Field::readonlyInteger(11),
                'created_at'       => Field::readonlyDateTime(),

                'cart_id'          => Field::editableInteger(11),
                'delivery_details' => Field::editableString(64*1024),
                'first_name'       => Field::editableString(64*1024),
                'last_name'        => Field::editableString(64*1024),
                'address'          => Field::editableString(64*1024),
                'city'             => Field::editableString(64*1024),
                'email'            => Field::editableString(64*1024)
                
                
            ];
        }

        public function getAllByCartId(int $cartId): array {
            $items = $this->getAllByFieldName('cart_id', $cartId);

           usort($items, function($a, $b) {
                return strcmp($a->created_at, $b->created_at);
           });
           
           return $items;
        }
    }