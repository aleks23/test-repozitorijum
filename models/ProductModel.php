<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class ProductModel extends Model {
        protected function getFields(): array {
            return [
                'product_id'       => new Field((new NumberValidator())->setIntegerLength(11), false ),
                'created_at'       => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'title'            => new Field((new StringValidator())->setMaxLength(255) ),
                'description'      => new Field((new StringValidator())->setMaxLength(64*1024) ),
                'ingredients'      => new Field((new StringValidator())->setMaxLength(64*1024) ),
                'price'            => new Field((new NumberValidator())->setDecimal()
                                                                       ->setUnsigned()
                                                                       ->setIntegerLength(7)
                                                                       ->setMaxDecimalDigits(2) ),
                'category_id'      => new Field((new NumberValidator())->setIntegerLength(11) ),
                'user_id'          => new Field((new NumberValidator())->setIntegerLength(11) ),
                'image_path'       => new Field((new StringValidator())->setMaxLength(64*1024) )
                
                
            ];
        }
        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }

        public function getAllBySearch(string $keywords) {
            $sql = 'SELECT * FROM `product` WHERE `title` LIKE ? OR `description` LIKE ?;';

            $keywords = '%' . $keywords . '%';

            $prep = $this->getConnection()->prepare($sql);
            if (!$prep) {
                return [];
            }

            $res = $prep->execute([$keywords, $keywords]);
            if (!$res) {
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
    }

   