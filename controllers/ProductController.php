<?php       
    namespace App\Controllers;

    class ProductController extends \App\Core\Controller {
        public function show($id) {
            $productModel = new  \App\Models\ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($id);

            if (!$product) {
                header('Location: /');
                exit;
            }

            $this->set('product', $product); 

            $productViewModel = new \App\Models\ProductViewModel($this->getDatabaseConnection());

            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');

            $productViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress, 
                    'user_agent' => $userAgent
                ]
            );

           
        } 

        private function normaliseKeywords(string $keywords): string {
            $keywords = trim($keywords);
            $keywords = preg_replace('/ +/', ' ', $keywords);
            # ...
            return $keywords;
        }
        
        public function postSearch() {
            $productModel = new  \App\Models\ProductModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = $this->normaliseKeywords($q);

            $product = $productModel->getAllBySearch($q);

            $this->set('product', $product);
        }
       
    }